$(document).ready(function() {
	$(".slide-one").owlCarousel({
	    loop:true,
	    nav:false,
	    navigation:false,
	    autoplay:true,
	    autoplayTimeout:2000,
	    navText:["",""],
	     responsive:{
	        0:{
	            items:2
	        },
	        400:{
	            items:3
	        },
	        700:{
	        	items:4

	        },
	        1000:{
	            items:5
	        }
	    }
	});

	$(".slide-two").owlCarousel({
		    loop:true,
		    nav:true,
		    navigation:true,
		        navText: [$('.am-prev'),$('.am-next')],
		    margin:30,
		    autoplay:false,
		  responsive:
		  {
		        0:{
		            items:1
		        },
		        400:{
		            items:2
		        },
		        700:{
		        	items:3
		        },
		        1000:{
		            items:4
		    }
		}
	});

	$('#focus').lavalamp({
	
	});
});